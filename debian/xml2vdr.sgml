<!doctype refentry PUBLIC "-//OASIS//DTD DocBook V4.1//EN" [

<!-- Process this file with docbook-to-man to generate an nroff manual
     page: `docbook-to-man manpage.sgml > manpage.1'.  You may view
     the manual page with: `docbook-to-man manpage.sgml | nroff -man |
     less'.  A typical entry in a Makefile or Makefile.am is:

manpage.1: manpage.sgml
	docbook-to-man $< > $@

    
	The docbook-to-man binary is found in the docbook-to-man package.
	Please remember that if you create the nroff version in one of the
	debian/rules file targets (such as build), you will need to include
	docbook-to-man in your Build-Depends control field.

  -->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Alastair</firstname>">
  <!ENTITY dhsurname   "<surname>McKinstry</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>July 17, 2002</date>">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>mckinstry@computer.org</email>">
  <!ENTITY dhusername  "Alastair McKinstry">
  <!ENTITY dhucpackage "<refentrytitle>XML2VDR</refentrytitle>">
  <!ENTITY dhpackage   "xml2vdr">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
        </author>
    <copyright>
      <year>2002</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>Convert XML DVB-SI files to a VDR channel list</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage; <replaceable>filename.xmlstat</replaceable>
      </command>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>
    <command>&dhpackage;</command> is a simple utility to convert XML DVB-SI files
    into a channel list for VDR.


    You can get DVB-SI output for a satellite by using dvbtune:

    dvbtune -i

    will generate the appropriate XML output to standard output.

        
  </refsect1>
  <refsect1>
    
  </refsect1>

  </refsect1>
  <refsect1>
    <title>OPTIONS</title>

    <para>	
    The following options are understood by <command>xml2vdr</command>:
    </para>

    <variablelist>

    <varlistentry>
    <term>--help</term>
    <listitem>
    Print help message
    </listitem>
    </varlistentry>

    <varlistentry>
    <term>--version</term>
    <listitem>
    Print version string
    </listitem>
    </varlistentry>
    </variablelist>

  
  <refsect1>
    <title>SEE ALSO</title>

    <para>dvbtune(1), vdr(1), szap(1).</para>

  </refsect1>
  <refsect1>
    <title>AUTHOR</title>

    <para>
    <command>&dhpackage;</command> was written by Dave Chapman
    <email>dave@dchapman.org</email>.    
    </para>


    <para>
    This manual page was written by &dhusername; &dhemail; for
      the &debian; system (but may be used by others).  Permission is
      granted to copy, distribute and/or modify this document under
      the terms of the <acronym>GNU</acronym> Free Documentation
      License, Version 1.1 or any later version published by the Free
      Software Foundation; with no Invariant Sections, no Front-Cover
      Texts and no Back-Cover Texts.</para>

  </refsect1>
</refentry>

<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-omittag:t
sgml-shorttag:t
sgml-minimize-attributes:nil
sgml-always-quote-attributes:t
sgml-indent-step:2
sgml-indent-data:t
sgml-parent-document:nil
sgml-default-dtd-file:nil
sgml-exposed-tags:nil
sgml-local-catalogs:nil
sgml-local-ecat-files:nil
End:
-->


